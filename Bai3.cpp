// Ho Ten: Dao Thi Tuyen
// MSV   : 12020418
// Lop   : INT 2022 4
// Bai 3 : Class MyString
#include <iostream>
#include <windows.h>
#include <cstring>

using namespace std;

class MyString{
	private:
		char *data;
	public:
		MyString(){
			data = NULL;
		}
		MyString(char *str){
			data = new char[strlen(str)+1];
			strcpy(data, str); 
		}
		void printString(){
			cout << data << endl;
		}
		void normalizingString(){
			
		    int index=0;
		    char *str= new char[100];
		    bool checked=false;
		    if (data[0]!=' ') {
		        str[index]=data[0];
		        index++;
		        checked = true;
		    }
		    for (int i=1; i<strlen(data); i++) {
		        if (data[i]!=' ') {
		            str[index] = data[i];
		            index++;
		            checked = true;
		        }
		        if (data[i]==' '&&checked) {
		            str[index]=data[i];
		            index++;
		            checked=false;
		        }
		    }
		    if(str[index-1]==' ')
		        str[index-1]='\0';
		    else
		        str[index]='\0';
		    
		    str[0]=toupper(str[0]);
		    for (int i=1; i<strlen(str); i++) {
		        str[i]= tolower(str[i]);
		        if ((str[i]!=' '&str[i-1]==' ')) {
		            str[i]=toupper(str[i]);
		        }
		    }
		    for (int i=0; i<=strlen(str); i++) {
		        data[i]=str[i];
		    }
		    cout<<str<<endl;
		}
		
		void split()
		{
			char * p = strtok (data," ");
			while (p!=0)
		  {
		    cout << p << '\n';
		    p = strtok(NULL," ");
		  }
		  delete[] data;
		}
		char *getString(){
			char *newString = new char[strlen(data)+1];
			strcpy(newString, data);
			return newString;
		}
		
		bool stringEqual(MyString *otherString){
			char *temp = otherString->getString();
			if(strcmp(data, temp) == 0){
				delete temp;
				return true;
			}
			else
			{
				delete temp;
				return false;
			}
		}
		void concentratingString(MyString *otherString){
			char *temp = otherString->getString();
			strcat(data, temp);
			cout << data << endl;
		}
		
		char *reverseString()
		{
		  char *tmp, i;
		 
		  i = 0;
		  tmp = (char *)malloc(strlen(data)+1);
		  while (i<strlen(data))
		    *(tmp+i) = *(data + strlen(data) - i++ - 1);
		    *(tmp+i) = 0;
		  return tmp;
		}
		
		void setString(char *str){
			delete data;
			data = NULL;
			
			data = new char [strlen(str)+1];
			strcpy(data, str);
		}
};

main()
{
	MyString *mystring = new MyString ("I am Nam");
	MyString *otherstring = new MyString ("I hate you");
	/* Enter string
	char *fullname = new char[30];
	cout << "Enter full name: ";
	cin.getline(fullname,30);
	cout << endl;
	*/ 
	/*
	MyString *stringName = new MyString (fullname);
	stringName->printString();
	stringName->normalizingString();
	stringName->split();
	*/
	//cout << "My string is " ;
	mystring->printString();
	mystring->normalizingString();
	mystring->split();
	
	// Compare two string
	if(mystring->stringEqual(otherstring)){
		cout << "Two strings are equal.";
	}
	else{
		cout << "Two strings are not equal.";
	}
	
	/*
	// concentratingString
	//otherstring->concentratingString(mystring);
	
	// Reversing string.
	cout << mystring->reverseString() << endl;
	/*
	// Set new value
	mystring->setString("UET");
	mystring->printString();
	*/
}
