// Ho Ten: Dao Thi Tuyen
// MSV   : 12020418
// Lop   : INT 2022 4
// Bai 1 : Class Date
#include <iostream>
#include <windows.h>
using namespace std;

int namNhuan (int y) {
	if( (y%400==0) || (y%4==0 && y%100!=0))
		return 1;
	else return 0;
}
int ngayCuaThang(int month, int year) {
	int kq;
	if ( month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12 )
		kq = 31;
	else if (month == 2) {
		if (namNhuan(year) == 1) kq = 29;
		else kq = 28;
	}
	else kq = 30;
	return kq;
}
class Date
{ 
	public:
		Date() {
			day = 1;
			month = 1;
			year = 1900;
		}
		Date(int x, int y, int z) {
			day = x;
			month = y;
			year = z;
		}
	
		Date(const Date &a)// constructor copy
		{
			day = a.day;
			month = a.month;
			year = a.year;
		}

		void setDay(int d){
			day = d;
		}
		void setMonth(int m) {
			month = m;
		};
		void setYear (int y) {
			year = y;
		}
		void setMonthWord(string s){
			switch('s') {
				case ('jan') :
					month =1;
					break;
				case ('feb') :
					month =2;
					break;
				case ('mar') :
					month =3;
					break;
			default: month = 0;
			}
		}
		int getDay(){return day;}
		int getMonth() {return month;}
		
		void output(){
			cout << "day=" <<day<< " month " << month <<endl;;
		}
		//private:
				int day;
				int month;
				int year;
};
int soNgay(Date &a, Date &b) {
	int count = 0;
	Date term();
	int i,j;
	if (a.year == b.year) {
		for (i = a.month; i < b.month ; i++) 
			count += ngayCuaThang(i,a.year);
		}
	if (a.year < b.year) {
		for (i = a.month; i <= 12; i++) 
			count += ngayCuaThang(i,b.year);
	for (i = 1; i < b.month ; i++) 
			count += ngayCuaThang(i,b.year);
		for (j = a.year; j < b.year; j++){
			if (namNhuan(j) == 1 ) count += 366;
			else count += 365;
		}
	}
	count = count + (b.day - a.day);
	return count;
}	
int main() {
	Date d1(1, 9, 2013),d2(25, 4, 2015);
	//d1.setDay(4);
	//d2.setDay(30);
	//d1.setMonth(2);
	cout << d1.getDay() << endl;
	d1.getMonth();
	//d2.setMonthWord("jan");
	cout << "So ngay " << soNgay(d1,d2) << endl;
	d1.output();
	d2.output();
	return 0;
}
