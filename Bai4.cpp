// Ho name: Dao Thi Tuyen
// MSV   : 12020418
// Lop   : INT 2022 4
// Bai 4 : 

#include <iostream>
#include <conio.h>
#include <stdio.h>
#include <string.h>
using namespace std;

class Student
{
    private:
        char* fullname;
		int yearborn;
        float a_mark;
    public:
        Student* Next;
        Student()
        {
            yearborn=0;
            a_mark=1;
            fullname=new char[25];
            Next=NULL;
        }
        void Enter()
        {
            cout<<"  Full name:";
            cin.getline(fullname,25);
			cout<<"  Year:";
            cin>>yearborn;
            cout<<"  Average Mark:";
            cin>>a_mark;
        }
        void setValue(char* name,int year,float mark)
        {
            fullname=name;
			yearborn=year;
            a_mark=mark;
        }
        int getyearborn()
        {
            return yearborn;
        }
        char* getfullname()
        {
            return fullname;
        }
        float geta_mark()
        {
            return a_mark;
        }
};
 
class MYCLASS
{
    private:
        Student* First;
    public:
        MYCLASS()
        {
            First=NULL;
        }
        void createNew();
        void swap(Student* st1,Student* st2);
        void sortbyName();
        void sortbyMark();
        void deleteStudent();
        void printList();
        void printListbyMark();
};
 
void MYCLASS::createNew()
{
    Student* Current;
    int numStudent=0;
    char c;
    do
    {
        if(First==NULL)
        {
            First=new Student;
            Current=First;
        }
        else
        {
            Current->Next=new Student;
            Current=Current->Next;
        }
        cout<<"Enter information of student "<<++numStudent<<":"<<endl;
        Current->Enter();
        cout<<"Continue ?(y/n):";
        fflush(stdin);c=getche();
        cout<<endl;
    }
    while(c=='y'||c=='Y');
}
 
void MYCLASS::sortbyName()
{
    if(First==NULL)
    {
        printf("List is null");
        fflush(stdin);getch();
        return;
    }
    Student* Currentcompare;
    Currentcompare=First;
    while(Currentcompare->Next!=NULL)
    {
        Student* compare;
        compare=Currentcompare->Next;
        while(compare!=NULL)
        {
            if(strcmp(compare->getfullname(),Currentcompare->getfullname())<0)
            {
                int ns=Currentcompare->getyearborn();
                char* s=Currentcompare->getfullname();
                float hsl=Currentcompare->geta_mark();
                Currentcompare->setValue(compare->getfullname(),compare->getyearborn(),compare->geta_mark());
                compare->setValue(s,ns,hsl);
            }
            compare=compare->Next;
        }
        Currentcompare=Currentcompare->Next;
    }
    cout<<"List student was sorted";
    fflush(stdin);getch();
}
//
void MYCLASS::sortbyMark()
{
    if(First==NULL)
    {
        printf("List is null");
        fflush(stdin);getch();
        return;
    }
    Student* Currentcompare;
    Currentcompare=First;
    while(Currentcompare->Next!=NULL)
    {
        Student* compare;
        compare=Currentcompare->Next;
        while(compare!=NULL)
        {
            if(compare->geta_mark() < Currentcompare->geta_mark())
            {
                int ns=Currentcompare->getyearborn();
                char* s=Currentcompare->getfullname();
                float hsl=Currentcompare->geta_mark();
                Currentcompare->setValue(compare->getfullname(),compare->getyearborn(),compare->geta_mark());
                compare->setValue(s,ns,hsl);
            }
            compare=compare->Next;
        }
        Currentcompare=Currentcompare->Next;
    }
    cout<<"List student was order by mark";
    fflush(stdin);getch();
}
//

 
void MYCLASS::deleteStudent()
{
    if(First==NULL)
    {
        printf("List is null");
        fflush(stdin);getch();
        return;
    }
    Student *Current;
    char *name = new char[25];
    cout<<"Name student to delete:";
    cin.getline(name, 25);
    if(strcmp(First->getfullname(), name)==0)
    {
        Current=First;
        First=First->Next;
        delete Current;
        return;
    }
    Current=First;
    while(Current->Next!=NULL){
    	if(strcmp(Current->Next->getfullname(), name)==0)
        {
            Student* xoa=Current->Next;
            Current->Next=xoa->Next;
            delete xoa;
            cout<<"Student was removed";
            return;
        }
        else Current=Current->Next;
    }  
    
    fflush(stdin);getch();
}

void MYCLASS::printListbyMark(){
	int a,b;
	cout << "Enter mark interval a vs b: "; cin >> a >> b;
	if(First==NULL)
    {
        printf("List is null");
        fflush(stdin);getch();
        return;
    }
    Student* Current;
    Current=First;
    if(Current!=NULL)
        printf("%-26s%-8s%-3s\n","Full name","Year ","Mark");
    while(Current!=NULL)
    {
        if (a <= (Current->geta_mark()) &&  (Current->geta_mark()) <= b){
        	printf("%-26s%-8i%-8.1f\n",Current->getfullname(),Current->getyearborn(),Current->geta_mark());
        }
        Current=Current->Next;
    }
    fflush(stdin);getch();
}
void MYCLASS::printList()
{
    if(First==NULL)
    {
        printf("List is null");
        fflush(stdin);getch();
        return;
    }
    Student* Current;
    Current=First;
    if(Current!=NULL)
        printf("%-26s%-8s%-3s\n","Full name","Year ","Mark");
    while(Current!=NULL)
    {
        printf("%-26s%-8i%-8.1f\n",Current->getfullname(),Current->getyearborn(),Current->geta_mark());
        Current=Current->Next;
    }
    fflush(stdin);getch();
}
 
main()
{
    MYCLASS Class1;
    int c;
    while(1)
    {
        //clrscr();
        cout<<"\t\tMENU"<<endl;
        cout<<"\t1.Create a list"<<endl;
        cout<<"\t2.Delete a student"<<endl;
        cout<<"\t3.Sorting list by name"<<endl;
        cout<<"\t4.Sorting list by mark"<<endl;
        cout<<"\t5.Print list"<<endl;
        cout<<"\t6.Print list by mark"<<endl;
        cout<<"\t7.Exit program"<<endl;
        cout<<"\n\tPlease choose function:";
        fflush(stdin);c=getche();
        cout<<endl;
        switch(c)
        {
            case '1':
                Class1.createNew();
                break;
            case '2':
                Class1.deleteStudent();
                break;
            case '3':
                Class1.sortbyName();
                break;
            case '4':
                Class1.sortbyMark();
                break;
            case '5':
                Class1.printList();
                break;
            case '6':
                Class1.printListbyMark();
                break;
            case '7':
                return 0;
        }
    }
}
